import { describe, expect, test } from '@jest/globals';
import client from '../lib/client/self';
import CONFIG from '../lib/attestations/config';

const sampleOmniAttester = {
  attestorDID: CONFIG.ATTESTOR_DID,
  claimType: 'name',
  claimData: 'batman',
  attestorPublicKey: CONFIG.PUBLIC_KEY,
  attestorPrivateKey: CONFIG.PRIVATE_KEY,
  attestorName: CONFIG.attestorName,
};

describe('Issue new claims', () => {
  test('Check unsupported chain', async () => {
    await expect(client.issueNewClaim({ blockchain: 'xyz' })).rejects.toThrow(
      'Chain not supported',
    );
  });
  test('Check Missing arguments error', async () => {
    await expect(client.issueNewClaim({ blockchain: 'omni' })).rejects.toThrow(
      'Missing arguments',
    );
  });
  test('Check invalid DID', async () => {
    await expect(
      client.issueNewClaim({
        blockchain: 'omni',
        requestorDID: 'aaa', // invalid requestorDID
        ...sampleOmniAttester,
      }),
    ).rejects.toThrow('Invalid DID');
  });

  test('Successful omni execution', async () => {
    const claim = await client.issueNewClaim({
      blockchain: 'omni',
      requestorDID: 'did:mdip:omni-xvtu-5yzp-qde8-3l5',
      ...sampleOmniAttester,
    });
    const parsedClaim = JSON.parse(claim);
    expect(parsedClaim).toHaveProperty('proof.type');
    expect(parsedClaim).toHaveProperty('proof.created');
    expect(parsedClaim).toHaveProperty('proof.proofPurpose');
    expect(parsedClaim).toHaveProperty('proof.verificationMethod');
    expect(parsedClaim).toHaveProperty('proof.jws');
  });
});
