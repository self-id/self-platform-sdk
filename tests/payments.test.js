import { describe, expect, test } from '@jest/globals';
import { getDidInputs } from '../lib/ipns/didHelpers';
import {
  prepTx, getUtxos, getSignedTx, getPreparedPayment,
} from '../lib/payments';

describe('prepTx function tests', () => {
  let didInputs = null;
  let utxos = null;
  let didTx = null;
  let signedTx = null;
  let blockchain = null;
  const network = 'testnet';
  // testnet mnemonic for testing purpose only
  const mnemonic = 'faculty thumb alpha climb warm victory grace joke copy dutch burst thank';
  test('getDidInputs success', async () => {
    didInputs = await getDidInputs(mnemonic);
    didInputs.data.network = network;
    expect(didInputs).toHaveProperty('data.didCreator');
  });

  test('getUtxos success', async () => {
    const {
      data: { didCreator },
    } = didInputs;
    ({ blockchain } = didInputs.data);
    utxos = await getUtxos(didCreator, network, blockchain);
    expect(utxos).toHaveProperty('unspents[0].satoshis');
  }, 10000);

  test('prepTx success', async () => {
    didInputs.data.nulldata = '/ipns/xyz';
    didInputs.data.bypassDocChecks = true;
    didTx = await prepTx({ didInputs: didInputs.data, utxos });
    expect(didTx).toBeTruthy();
  });

  test('signTx success', async () => {
    signedTx = await getSignedTx('xyz', didInputs);
    expect(signedTx.success).toBe(true);
  });

  test('getPreparedPayment success', async () => {
    const {
      data: { didCreator },
    } = didInputs;
    const preparedTx = await getPreparedPayment(
      didCreator,
      [{ address: 'mjEwNVeF3TF127WHLSG8Je9Db1hJrnxkJF', amount: 0.000001 }],
      { blockchain: 'btc', network, utxoData: utxos },
    );
    expect(preparedTx).toBeTruthy();
  });
});
