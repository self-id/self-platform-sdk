/** @type {import('@jest/types').Config.InitialOptions} */
module.exports = {
  rootDir: '..',
  moduleFileExtensions: ['js', 'json', 'ts'],
  testMatch: ['<rootDir>/tests/**/*.test.js'],
  setupFiles: ['dotenv/config'],
  testTimeout: 3200000,
  maxWorkers: 1,
  verbose: true,
};
