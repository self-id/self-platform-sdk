const path = require('path');
const { I18n } = require('i18n');

const { SUPPORTED_LOCALES, DEFAULT_LOCALE } = require('./lib/utils/constants');

const i18n = new I18n({
  locales: SUPPORTED_LOCALES,
  defaultLocale: DEFAULT_LOCALE,
  directory: path.join(__dirname, 'locales'),
});

module.exports = i18n.__;
