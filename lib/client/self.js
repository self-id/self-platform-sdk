const { randomBytes } = require('crypto');
const bitcoin = require('bitcoinjs-lib');
const bitcoinMessage = require('bitcoinjs-message');
const WEB3 = require('web3');

const { ALLOWED_CHAINS } = require('../config');

const client = exports;

/**
 * Validates a DID with basic checks
 * @param {string} DID
 * @returns {boolean}
 */
const validateDID = (DID) => {
  const [part1, part2, part3] = DID.split(':');
  if (
    part1 !== 'did'
    || part2 !== 'mdip'
    || !ALLOWED_CHAINS.includes(part3.split('-')[0])
  ) {
    return false;
  }
  return true;
};

/**
 * Method to issue a new Verifiable Claim.
 * @param {{
 *  blockchain: string
 *  attestorDID: string
 *  requestorDID: string
 *  claimType: string
 *  claimData: Object
 *  attestorName: string
 *  attestorPublicKey: string
 *  attestorPrivateKey: string
 *  givenNetwork: string
 *  expiry: Date || null
 *  atomCID: string || null
 * }} param0
 * @returns {Object}
 */
client.issueNewClaim = async ({
  blockchain,
  attestorDID,
  requestorDID,
  claimType,
  claimData,
  attestorName,
  attestorPublicKey,
  attestorPrivateKey,
  givenNetwork,
  expiry = null, // date object
  atomCID = null,
}) => {
  try {
    if (!ALLOWED_CHAINS.includes(blockchain)) {
      throw new Error('Chain not supported');
    }
    if (
      !attestorDID
      || !requestorDID
      || !claimType
      || !claimData
      || !attestorPublicKey
      || !attestorPrivateKey
      || !attestorName
    ) {
      throw new Error('Missing arguments');
    }
    // if (!credentialTypes.includes(claimType)) {
    //   throw new Error('claimType not supported');
    // }
    const isAttestorDIDValid = validateDID(attestorDID);
    const isRequestorDIDValid = validateDID(requestorDID);
    if (!isAttestorDIDValid || !isRequestorDIDValid) {
      throw new Error('Invalid DID');
    }
    let claim = {};
    const validFrom = new Date().toISOString();
    let validUntil = null;
    if (expiry?.toISOString() && new Date() < expiry) {
      validUntil = expiry.toISOString();
    } else {
      validUntil = new Date(
        Date.now() + 180 * 24 * 60 * 60 * 1000,
      ).toISOString(); // ~6 months expiry time
    }
    if (
      // (claimType === 'ageOver18'
      // || claimType === 'ageOver21'
      // || claimType === 'isPlatformXUser')
      // &&
      claimData[claimType]
    ) {
      claim = {
        '@context': ['https://www.w3.org/2018/credentials/v1'],
        id: attestorDID,
        type: ['VerifiableCredential', atomCID || 'general'],
        issuer: {
          id: attestorDID,
          name: attestorName,
        },
        issuanceDate: validFrom,
        expirationDate: validUntil,
        credentialSubject: {
          id: requestorDID,
          // [claimType]: true,
          schema: atomCID,
          [claimType]: claimData[claimType],
        },
      };
    }
    const message = JSON.stringify(claim);
    let signature;
    if (blockchain === 'eth') {
      const web3Instance = new WEB3();
      signature = await web3Instance.eth.accounts.sign(
        message,
        `0x${attestorPrivateKey}`,
      );
    } else if (blockchain === 'mongodb') {
      const network = bitcoin.networks.bitcoin;
      const keyPair = bitcoin.ECPair.fromPrivateKey(
        Buffer.from(attestorPrivateKey, 'hex'),
        { network },
      );
      signature = bitcoinMessage.sign(
        message,
        Buffer.from(attestorPrivateKey, 'hex'),
        keyPair.compressed,
        { extraEntropy: randomBytes(32) },
      );
    } else {
      let network = bitcoin.networks.testnet;
      if (givenNetwork === 'mainnet') {
        network = bitcoin.networks.bitcoin;
      }
      const keyPair = bitcoin.ECPair.fromWIF(attestorPrivateKey, network);
      const obatinedPrivKey = keyPair.privateKey;

      signature = bitcoinMessage.sign(
        message,
        obatinedPrivKey,
        keyPair.compressed,
        { extraEntropy: randomBytes(32) },
      );
    }

    claim.proof = {
      type: 'EcdsaSecp256k1VerificationKey2019',
      created: validFrom,
      proofPurpose: 'assertionMethod',
      verificationMethod: attestorPublicKey,
      jws: signature.toString('base64'),
    };
    return JSON.stringify(claim);
  } catch (errorINC) {
    throw new Error(errorINC.message);
  }
};
