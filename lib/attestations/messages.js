module.exports = {
  ERROR_FETCHING_FROM_IPFS: 'Error in fetching data from IPFS',
  ERROR_UPLOADING_TO_IPFS: 'Error in uploading to IPFS try again later.',
  ERROR_DECRYPTING_DATA: 'Error in decrypting data',
  VP_NOT_VERIFIED: 'Could not verify VP',
  EXPIRED_VP: 'VP has expired atoms',
  INVALID_ISSUANCE_DATE: 'Invalid atom issuance date',
  UNEXPECTED_ERROR: 'UNEXPECTED_ERROR',
};
