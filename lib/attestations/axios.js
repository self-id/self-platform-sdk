const axios = require('axios');
const CONFIG = require('./config');

const getMarketPlaceAxiosConfig = (
  endpoint,
  data = {},
  method = 'POST',
  headers = {},
) => ({
  method,
  url: `${CONFIG.SELF_MARKETPLACE_URL}${endpoint}`,
  data,
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json',
    ...headers,
  },
});

const fetchResponse = async (config) => {
  try {
    const response = await axios(config);
    return {
      status: response?.status,
      data: response?.data,
    };
  } catch (err) {
    return {
      status: err.response?.status,
      data: err.response?.data,
    };
  }
};

const getMarketPlaceResponse = async (
  endpoint,
  data = {},
  method = 'POST',
  headers = {},
) => {
  const config = getMarketPlaceAxiosConfig(endpoint, data, method, headers);
  return fetchResponse(config);
};

module.exports = {
  fetchResponse,
  getMarketPlaceAxiosConfig,
  getMarketPlaceResponse,
};
