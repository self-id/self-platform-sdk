const debug = require('debug')('self-sdk:atoms');
const { encryptUploadIPFS } = require('../ipns/helper');
const CONFIG = require('./config');

/**
 * Function to convert JSON object to array of objects with name & value
 * @param {*} userData
 * @returns
 */
const getUserMolecule = (userData) => Object.keys(userData).map((key) => ({
  name: key,
  value: userData[key],
}));

/**
 * Function to encapsulate data with attestor information
 * This is done so each claim could have information of attestor
 * @param {*} data
 * @returns
 */
const getMoleculeData = (data) => ({
  atoms: data,
  attestorName: CONFIG.attestorName,
  attestorDID: CONFIG.ATTESTOR_DID,
  attestorPublicKeyHex: CONFIG.PUBLIC_KEY_HEX,
});

/**
 * Function to upload `userData` to IPNS after encrypting it with `publickey`
 * @param {*} userData
 * @param {*} publickey
 * @param {*} ipnsHelper
 * @returns
 */
const saveMoleculesToIPNS = async (userData, publickey, ipnsHelper) => {
  const molecule = userData;
  debug('[saveMoleculesToIPNS]', JSON.stringify(molecule));
  debug('[userData]', userData);
  debug('[publickey]', publickey);
  if (molecule.length) {
    const data = getMoleculeData(molecule);
    debug('[saveMoleculesToIPNS]', JSON.stringify(data));
    return encryptUploadIPFS(data, publickey, ipnsHelper);
  }
  return null;
};

module.exports = {
  getUserMolecule,
  saveMoleculesToIPNS,
};
