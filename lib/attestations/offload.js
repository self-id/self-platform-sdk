const debug = require('debug')('self-sdk:atttestations');
const { issueNewClaim } = require('../client/self');
const { encryptUploadIPFS } = require('../ipns/helper');
const { offLoadingCID } = require('./onboarding');
const {
  convertCIDArrayAvailableMolecules,
  getAttesterDIDDoc,
} = require('./cid');
const {
  blockchain,
  attestorName,
  PUBLIC_KEY,
  PRIVATE_KEY,
  ATTESTOR_DID,
} = require('./config');
const { ERROR_UPLOADING_TO_IPFS } = require('./messages');

/**
 * Function to get available Molecules object
 * @returns
 */
const getAvailableMoleculeCIDObject = async () => {
  try {
    const ipnsData = await getAttesterDIDDoc();
    const cidObj = await convertCIDArrayAvailableMolecules(
      ipnsData?.availableMolecules || [],
    );
    return cidObj;
  } catch (error) {
    debug('getAvailableMoleculeCIDObject error', error);
    return {};
  }
};

/**
 * Function to  get CID of current atom
 * @param {*} cidObj
 * @param {*} claimType
 * @returns
 */
const getAvailableAtomCID = (cidObj, claimType) => {
  const objArr = Object.values(cidObj);
  const currentAtomDef = objArr.find(({ name }) => claimType === name);
  if (currentAtomDef?.cid) {
    return currentAtomDef.cid;
  }
  return null;
};

/**
 * Function to offload single claim
 * @param {*} param0
 * @param {*} param1
 * @param {*} claimData
 * @param {*} ipnsHelper
 * @param {*} offload
 * @returns
 */
const offloadSingleClaim = async (
  { claimType, requestorDID, publicKey },
  claimData,
  ipnsHelper,
  offload = true,
  expiry = null,
  atomCID = null,
) => {
  try {
    debug('offloading claimData', claimData);
    const issuedClaim = await issueNewClaim({
      blockchain, // omni
      requestorDID,
      claimType,
      claimData,
      attestorName,
      attestorPublicKey: PUBLIC_KEY,
      attestorPrivateKey: PRIVATE_KEY,
      attestorDID: ATTESTOR_DID,
      expiry,
      atomCID,
    });
    debug('issuedClaim', JSON.stringify(issuedClaim));
    const cid = await encryptUploadIPFS(issuedClaim, publicKey, ipnsHelper);
    if (cid && offload) {
      await offLoadingCID(requestorDID, cid);
    }
    return cid;
  } catch (e) {
    debug('offloadSingleClaim error :>> ', e);
    return null;
  }
};

/**
 * Function to offload each key value pair of `claimData`
 * as granular attestation by calling `offloadSingleClaim`
 * @param {*} param0 { requestorDID, publicKey }
 * @param {*} claimData
 * @param {*} ipnsHelper
 * @param {*} offload
 * @returns
 */
const offloadClaims = async (
  { requestorDID, publicKey },
  claimData,
  ipnsHelper,
  offload = true,
  expiry = null,
) => {
  const cids = [];
  const availableMolDef = await getAvailableMoleculeCIDObject();
  const promises = Object.keys(claimData).reduce(
    async (promise, objKey) => promise.then(async () => {
      const atomCID = getAvailableAtomCID(availableMolDef, objKey);
      // if atomCID is null (not present in availableMolecules) &
      // it is not "general" atom just dont offload the atom
      if (objKey.toLocaleLowerCase() !== 'general' && atomCID === null) {
        debug(
          `${objKey} is not defined in available molecules. kindly add it from onboarding portal.`,
        );
        return;
      }
      const cid = await offloadSingleClaim(
        { claimType: [objKey], requestorDID, publicKey },
        { [objKey]: claimData[objKey] },
        ipnsHelper,
        offload,
        expiry,
        atomCID,
      );
      cids.push(cid);
    }),
    Promise.resolve(),
  );
  await Promise.all([promises]);
  return cids.filter((cid) => cid !== null);
};

/**
 * Function to offload attestations so self app can fetch the attestations
 * @param {*} param0 { publicKey, requestorDID }
 * @param {*} claimData
 * @param {*} ipnsHelper
 * @param {*} offload
 * @returns
 */
const sendAttestation = async (
  { publicKey, requestorDID },
  claimData,
  ipnsHelper,
  offload = true,
  expiry = null,
) => {
  debug('requestorDID', requestorDID);
  const availableMolDef = await getAvailableMoleculeCIDObject();
  const objArr = Object.keys(availableMolDef);
  let response = {};
  if (objArr.length === 0) {
    debug('You have not defined any atom definitions on the onboarding portal');
  }
  try {
    const cids = await offloadClaims(
      { requestorDID, publicKey },
      claimData,
      ipnsHelper,
      offload,
      expiry,
    );
    debug('all cids', cids);
    // if available molecules is set & still there is no
    // CID offloaded then return error ERROR_UPLOADING_TO_IPFS
    if (!cids.length && objArr.length > 0) {
      debug('Error in uploading to IPFS try again later');
      response = {
        success: false,
        data: null,
        error: ERROR_UPLOADING_TO_IPFS,
      };
    } else {
      response = { success: true, data: { claims: cids }, error: null };
    }
  } catch (e) {
    debug('sendAttestation error :>> ', e);
    response = { success: false, data: null, error: e.toString() };
  }
  return response;
};

module.exports = {
  sendAttestation,
};
