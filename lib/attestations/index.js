const { verifyAttestations } = require('./flow');
const { sendAttestation } = require('./offload');
const {
  checkRequiredForLogin,
  getAttesterDIDDoc,
} = require('./cid');

module.exports = {
  verifyAttestations,
  sendAttestation,
  checkRequiredForLogin,
  getAttesterDIDDoc,
};
