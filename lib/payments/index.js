const { signTx, preparePayment } = require('@mdip/client/lib/client/mdip');
const debug = require('debug')('self-sdk:payments/index.js');
const axios = require('axios');
const { SELF_BACKEND } = require('../attestations/config');

const prepTx = async (inputs) => {
  const data = JSON.stringify(inputs);
  const config = {
    method: 'post',
    url: `${SELF_BACKEND}/prepare_transaction`,
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  };

  return new Promise((resolve, reject) => {
    axios(config)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
const ethFaucet = async (creator) => {
  const data = JSON.stringify({ creator });
  const config = {
    method: 'post',
    url: `${SELF_BACKEND}/eth_faucet`,
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  };

  return new Promise((resolve, reject) => {
    axios(config)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const broadcastTx = async (signedTx, blockchain, network) => {
  const data = JSON.stringify({ signedTx, blockchain, network });

  const config = {
    method: 'post',
    url: `${SELF_BACKEND}/broadcast`,
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  };

  return new Promise((resolve, reject) => {
    axios(config)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const getUtxoSingle = async (didCreator, network, blockchain) => {
  const data = JSON.stringify({ didCreator, network, blockchain });

  const config = {
    method: 'post',
    url: `${SELF_BACKEND}/get_utxos`,
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  };

  return new Promise((resolve, reject) => {
    axios(config)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const getUtxos = async (
  didCreator,
  network,
  blockchain,
  timeOutSeconds = 40,
) => {
  const utxos = await getUtxoSingle(didCreator, network, blockchain).catch(
    async (err) => {
      debug(`getUtxoSingle error ${err}`);
      await new Promise((r) => setTimeout(r, timeOutSeconds * 1e3));
      const utxoResponse = await getUtxos(didCreator, network, blockchain);
      return utxoResponse;
    },
  );
  return utxos;
};
const getSignedTx = async (
  ipnsLink,
  DIDInputs,
  savedUtxos = { unspents: [] },
) => {
  const { blockchain, network } = DIDInputs.data;
  const utxos = savedUtxos.unspents.length
    ? savedUtxos
    : await getUtxos(DIDInputs.data.didCreator, network, blockchain);
  const didInputs = {
    ...DIDInputs.data,
    nulldata: `/ipns/${ipnsLink}`,
    bypassDocChecks: true,
  };

  const didTx = await prepTx({
    didInputs,
    utxos,
  });

  const creator = DIDInputs.keyPairs[0];
  const signedTx = await signTx(
    creator.privateKey,
    didTx,
    creator.publicKey,
    didInputs,
  );
  return signedTx;
};
const makeTransaction = async (
  ipnsLink,
  DIDInputs,
  network,
  blockchain,
  savedUtxos = { unspents: [] },
) => {
  const signedTx = await getSignedTx(ipnsLink, DIDInputs, savedUtxos);
  const broadcasted = await broadcastTx(signedTx.data, blockchain, network);
  return broadcasted;
};

const getPreparedPayment = async (
  fromAddr,
  toAddresses,
  opts,
) => preparePayment(fromAddr, toAddresses, opts);

module.exports = {
  getPreparedPayment,
  makeTransaction,
  getSignedTx,
  getUtxos,
  prepTx,
  ethFaucet,
  broadcastTx,
  signTx,
  preparePayment,
};
