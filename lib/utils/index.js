const bitcoin = require('bitcoinjs-lib');
const bitcoinMessage = require('bitcoinjs-message');
const WEB3 = require('web3');

const { ETH_BLOCKCHAIN, PRIVATE_DB_MONGO } = require('../config');

const utils = exports;

/**
 * Function to verify a given signature is valid or not.
 * @param {string} message
 * @param {string} publicKey
 * @param {string} signature
 * @param {string} blockchain
 * @returns {boolean}
 */
utils.verifySign = async (message, publicKey, signature, blockchain) => {
  let localPubKey = publicKey;
  if (blockchain === ETH_BLOCKCHAIN) {
    const web3 = new WEB3();
    const signerPubKey = await web3.eth.accounts.recover(message, signature);
    if (signerPubKey === localPubKey) {
      return true;
    }
    return false;
  }
  if (blockchain === PRIVATE_DB_MONGO) {
    const network = bitcoin.networks.bitcoin;
    localPubKey = bitcoin.payments.p2pkh({
      pubkey: Buffer.from(localPubKey, 'hex'),
      network,
    }).address;
  }
  const verify = bitcoinMessage.verify(message, localPubKey, signature);
  return verify;
};

/**
 * Method to extract chains from a given Verifiable Credential
 * @param {Object} vc
 * @returns {Object}
 */
const extractChains = (credential) => {
  const chainInfo = {};
  let vc = credential;
  if (credential.verifiableCredential) {
    vc = credential.verifiableCredential[0];
  }
  const attestorDID = vc && vc.issuer && vc.issuer.id;
  const requestorDID = vc && vc.credentialSubject && vc.credentialSubject.id;
  if (attestorDID) {
    const [chain] = attestorDID.split('did:mdip:')[1].split('-');
    chainInfo.attestor = chain;
  }
  if (requestorDID) {
    const [chain] = requestorDID.split('did:mdip:')[1].split('-');
    chainInfo.requestor = chain;
  }
  return chainInfo;
};

utils.helpers = {
  extractChains,
};
