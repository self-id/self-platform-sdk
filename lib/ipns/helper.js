const IPFS = require('@mdip/client/lib/client/utils/ipfs');
const debug = require('debug')('self-sdk:ipns/helpers.js');
const { timeoutExecute } = require('./timeout');
const { IPFS_RECONNECT_MINUES, IPFS_NODE, IPFS_PORT } = require('./config');

class IPNSHelper {
  constructor() {
    this.connected = false;
    this.ctrl = null;
    this.res = null;
    this.url = null;
    this.wsPort = null;
    this.lastConnected = new Date();
    this.restarting = false;
  }

  async initializeIPNS(url = IPFS_NODE, wsPort = IPFS_PORT) {
    this.ctrl = new IPFS({ url, wsPort });
    return this.ctrl
      .connect()
      .then((res) => {
        this.res = res;
        this.url = url;
        this.wsPort = wsPort;
        this.lastConnected = new Date();
        this.connected = true;
        debug(`initializeIPNS success ${JSON.stringify(res)}`);
        return res;
      })
      .catch(async (error) => {
        debug(`initializeIPNS error ${error}`);
        if (`${error}`.includes('LockExistsError')) {
          return this.initializeIPNS();
        }
        return null;
      });
  }

  async checkConnected() {
    try {
      const { id: clientPeerId } = await this.ctrl.ipfsHttpClient.id();
      debug(`IPNSHelper clientPeerId ${clientPeerId}`);
      debug(`IPNSHelper ipfsNodeAddrPrefix ${this.ctrl.ipfsNodeAddrPrefix}`);
      const ipfsNodeAddr = `${this.ctrl.ipfsNodeAddrPrefix}${clientPeerId}`;
      const peers = await this.ctrl.coreClient.swarm.peers();
      debug(`IPNSHelper connected peers ${peers.length}`);
      debug(`IPNSHelper ipfsNodeAddr ${ipfsNodeAddr}`);
      const isPeerConnected = peers.find((peer) => ipfsNodeAddr.endsWith(peer.peer));
      if (isPeerConnected) {
        return true;
      }
    } catch (err) {
      debug(`IPNSHelper checkConnected error ${err}`);
    }
    return false;
  }

  async reconnect() {
    debug(`IPNS reconnecting at ${new Date()}`);
    debug(`reconnect url ${this.url} wsPort ${this.wsPort}`);
    this.connected = false;
    this.restarting = true;
    try {
      await this.ctrl.disconnect();
    } catch (err) {
      debug(`reconnect error ${err}`);
    }
    try {
      const res = await Promise.all([this.initializeIPNS()]);
      debug(`reconnect res ${JSON.stringify(res)}`);
    } catch (err) {
      debug(`reconnect error ${err}`);
    }
    this.restarting = false;
  }

  async checkAndReconnect() {
    const refreshTime = new Date(
      this.lastConnected.getTime() + IPFS_RECONNECT_MINUES * 60000,
    );
    if (refreshTime <= new Date()) {
      await Promise.all([this.reconnect()]);
      await Promise.all([this.waitReconnected()]);
    }
  }

  async waitReconnected(count = 0) {
    if (this.restarting) {
      await new Promise((r) => setTimeout(r, 100));
      await Promise.all([this.waitReconnected(count + 1)]);
      return false;
    }
    debug(`waitReconnected waited ${count * 100} ms`);
    return true;
  }

  async notConnectedReset() {
    if (!this.connected) {
      await Promise.all([this.reconnect()]);
      await Promise.all([this.waitReconnected()]);
    }
  }

  async checkInitialized() {
    await Promise.all([this.waitReconnected()]);
    debug(`checkInitialized connected ${this.connected}`);
    debug(`checkInitialized res ${JSON.stringify(this.res)}`);
    await Promise.all([this.notConnectedReset()]);
    if (!this.connected) {
      throw new Error('IPNS not initialized');
    }
  }

  async saveDataToIPFSDoc(data) {
    await this.checkInitialized();
    const newDIDDoc = { ...data };
    let cid = null;
    try {
      await timeoutExecute(async () => {
        cid = await this.ctrl.sendJSONToIPFS([newDIDDoc]);
      });
    } catch (err) {
      debug(`saveDataToIPFSDoc timeoutExecute error ${err}`);
      await Promise.all([this.reconnect()]);
      await Promise.all([this.waitReconnected()]);
      cid = await this.ctrl.sendJSONToIPFS([newDIDDoc]);
    }
    debug(`saveDataToIPFSDoc cid ${JSON.stringify(cid)}`);
    return cid[0].path;
  }

  async encryptData(claim, publicKey) {
    await this.checkInitialized();
    try {
      return this.ctrl.encryptContent([{ content: claim, publicKey }]);
    } catch (err) {
      await Promise.all([this.reconnect()]);
      await Promise.all([this.waitReconnected()]);
      return this.ctrl.encryptContent([{ content: claim, publicKey }]);
    }
  }

  async decryptData(data, key) {
    await this.checkInitialized();
    let decData = null;
    try {
      decData = await this.ctrl.decryptContent(data, [key]);
    } catch (err) {
      await Promise.all([this.reconnect()]);
      await Promise.all([this.waitReconnected()]);
      decData = await this.ctrl.decryptContent(data, [key]);
    }

    return decData[0];
  }

  async convertIPFSCidToData(cid) {
    await this.checkInitialized();
    let encData = null;
    try {
      await timeoutExecute(async () => {
        encData = await this.ctrl.getData(cid);
      });
    } catch (err) {
      debug(`convertIPFSCidToData timeoutExecute error ${err}`);
      await Promise.all([this.reconnect()]);
      await Promise.all([this.waitReconnected()]);
      encData = await this.ctrl.getData(cid);
    }
    return encData;
  }
}

const encryptUploadIPFS = async (data, publicKey, ipnsHelper) => {
  try {
    debug('[encryptUploadIPFS]', data, publicKey);
    const encryptedData = await ipnsHelper.encryptData(data, publicKey);
    return await ipnsHelper.saveDataToIPFSDoc(encryptedData);
  } catch (err) {
    debug('[encryptUploadIPFS]', err);
    return null;
  }
};

const fetchIPFSDecryptData = async (cid, privateKey, ipnsHelper) => {
  let error = null;
  let encryptedData = null;
  try {
    encryptedData = await ipnsHelper.convertIPFSCidToData(cid);
  } catch (err) {
    error = 'Error in fetching data from IPFS';
    debug('convertIPFSCidToData error >>', err);
    return { error, data: null, encryptedData: null };
  }
  const parsedData = JSON.parse(encryptedData);
  debug('encryptedData data >>', JSON.stringify(parsedData));
  try {
    const data = await ipnsHelper.decryptData(parsedData, privateKey);
    return {
      encryptedData,
      data,
      error,
    };
  } catch (err) {
    debug('decryptData error >>', err);
    error = 'Error in decrypting data';
    return { error, data: null, encryptedData: null };
  }
};

module.exports = {
  IPNSHelper,
  encryptUploadIPFS,
  fetchIPFSDecryptData,
};
