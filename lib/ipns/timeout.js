const { IPFS_TIMEOUT } = require('./config');

const timeoutExecute = async (myFunc, maxTime = IPFS_TIMEOUT) => new Promise((resolve, reject) => {
  (async () => {
    let timer = 0;
    const interval = setInterval(() => {
      timer += 100;
      if (timer >= maxTime) {
        clearInterval(interval);
        reject(new Error('Execution Timout'));
      }
    }, 100);
    try {
      const response = await myFunc();
      clearInterval(interval);
      resolve(response);
    } catch (err) {
      clearInterval(interval);
      reject(err);
    }
  })();
});

module.exports = {
  timeoutExecute,
};
