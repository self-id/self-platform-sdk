const { btcDID } = require('@mdip/client');
const { wallet: HD } = require('@mdip/client');

const { prepareInputs } = btcDID;

const prepareInputsOmni = async (
  masterRoot,
  childIndex = [0, 1],
  blockChain = 'omni',
  network = 'mainnet',
) => {
  const didInputs = await prepareInputs(
    blockChain,
    masterRoot,
    childIndex,
    network,
  );
  return didInputs;
};

const prepareInputsIPFS = async (
  masterRoot,
  childIndex = 1,
  blockChain = 'ipfs',
  network = 'mainnet',
) => {
  const childIndexesIPFS = [childIndex];
  const ipfsInputs = await prepareInputs(
    blockChain,
    masterRoot,
    childIndexesIPFS,
    network,
  );
  return ipfsInputs;
};

const getDidInputs = async (
  mnemonic,
  blockChain = 'omni',
  network = 'testnet',
) => {
  const seedBuffer = HD.generateSeed(mnemonic);
  const masterRoot = HD.obtainMasterRoot(seedBuffer);
  return prepareInputsOmni(masterRoot, [0, 1], blockChain, network);
};

module.exports = {
  prepareInputsIPFS,
  prepareInputsOmni,
  getDidInputs,
};
