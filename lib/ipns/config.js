module.exports = {
  IPFS_RECONNECT_MINUES: parseInt(process.env.IPFS_RECONNECT_MINUES, 10) || 60,
  IPFS_NODE: process.env.IPFS_NODE || 'https://beta-ipfs.yourself.dev',
  IPFS_PORT: process.env.IPFS_PORT || 443,
  IPFS_TIMEOUT: parseInt(process.env.IPFS_TIMEOUT, 10) || 20000,
};
